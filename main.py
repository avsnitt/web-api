import pymongo
import bson
import json
import requests
import os
import pprint

client = pymongo.MongoClient(CONFIG.get("mongoConnectionString"))
newline = "\r\n"
headers = {
    "Content-Type": "application/json"
}
ttvdb_token = requests.post("https://api.thetvdb.com/login", data=json.dumps({
    "apikey": CONFIG.get("apikey"),
    "userkey": CONFIG.get("userkey"),
    "username": CONFIG.get("username")
}), headers=headers).json()["token"]
headers["Authorization"] = f"Bearer {ttvdb_token}"

def new_show():
    os.system("cls")
    show_name = input("What is the show called: ")
    print("Searching...\n")
    search = requests.get(f"https://api.thetvdb.com/search/series?name={show_name}",
                          headers=headers).json()
    data = search["data"]
    if(len(data) == 1):
        data = data[0]
        print(f"Found show {data['seriesName']}: {data['overview']}")
        is_wanted_show = input("Do you want this show? (y/n)")
        if(is_wanted_show == "y"):
            wanted = data
        else:
            new_show()
    elif(len(data) > 1):
        print("Found the following results: ")
        results = [f"{str(data.index(i) + 1)}) {i['seriesName']}: {i['overview'].strip(newline)}" for i in data]
        print("\n".join(results))
        wanted_index = input("Which one would you like to add: ")
        wanted = data[int(wanted_index) - 1]
    show = requests.get(f"https://api.thetvdb.com/series/{wanted['id']}", headers=headers).json()
    summary = requests.get(f"https://api.thetvdb.com/series/{wanted['id']}/episodes/summary", headers=headers).json()["data"]
    episodes = requests.get(f"https://api.thetvdb.com/series/{wanted['id']}/episodes", headers=headers).json()["data"]
    show_oid = bson.objectid.ObjectId()
    if(int(summary["airedEpisodes"]) >= 99):
        print("Show has too many episodes.")
        print(episodes[-1])
        input()
        main_menu()

    seasons = {}
    season_oids = []
    for episode in episodes:
        if(episode["airedSeason"] == 0):
            pass
        else:
            if(seasons.get(episode["airedSeason"])):
                seasons[episode["airedSeason"]]["totalEpisodes"] += 1
            else:
                new_oid = bson.objectid.ObjectId()
                season_oids.append(new_oid)
                seasons[episode["airedSeason"]] = {
                    "_id": new_oid,
                    "show": show_oid,
                    "totalEpisodes": 1,
                    "season": int(episode["airedSeason"]),
                    "haveEpisodes": 0,
                    "fullSeasonDownload": False,
                    "individualDownloads": False,
                    "grabbed": False
                }

    showDoc = {
        "_id": show_oid,
        "ttvdbId": wanted["id"],
        "showName": wanted["seriesName"],
        "totalEpisodes": int(summary["airedEpisodes"]),
        "haveEpisodes": 0,
        "seasons": season_oids
    }
    client.media.shows.insert_one(showDoc)
    client.media.seasons.insert([seasons[s] for s in seasons])


def search_show():
    show_search = input("Please enter a search string for the show: ")
    s = list(client.media.shows.find(
        {
            "showName": {
                "$regex": show_search}
            }
        ))
    return s


def new_season_download():
    os.system("cls")
    shows = search_show()
    if(len(shows) > 1):
        print("Found more than one result, please be more specific")
        names = [i["showName"] for i in shows]
        print("\n".join(names))
        new_season_download()
    elif(len(shows) == 1):
        name = shows[0]["showName"]
        show_id = shows[0]["_id"]
        print(f"Found show {name}!")
        seasons = []
        for oid in shows[0]["seasons"]:
            s = client.media.seasons.find_one({"_id": oid})
            if not(s["grabbed"]):
                seasons.append(s)
        season_ids = [str(i["season"]) for i in seasons]
        print("The following seasons are available: ")
        print(", ".join(season_ids))
        selected_season = input("What season do you want to add a download for: ")
        if(selected_season in season_ids):
            magnet_link = input(f"Please enter the magnet link for {name}, Season {selected_season}: ")
            season_oid = seasons[season_ids.index(selected_season)]["_id"]
            download_oid = bson.objectid.ObjectId()
            doc = {
                "_id": download_oid,
                "type": "tv-show",
                "show": show_id,
                "season": season_oid,
                "fullSeason": True,
                "status": "notAdded",
                "url": magnet_link
            }

            client.media.seasons.update_one({"_id": season_oid}, {
                                "$set": {"fullSeasonDownload": download_oid,
                                "grabbed": True}})
            download_id = client.media.downloads.insert_one(doc)
            print("Successfully added new download! :) It will be sent to your download client soon!")
            new_season_download()

    else:
        print("Show wasn't found")
        new_season_download()


def new_episode_download():
    pass


def new_download():
    os.system("cls")
    choices = [
            "1) New season download",
            "2) New episode download",
        ]
    funcs = {
        "1": new_season_download,
        "2": new_episode_download,
        }
    choices_str = "\n".join(choices)
    menu_lines = [
                       "=== New download ===",
                       choices_str,
                       "==="*5]
    menu_string = "\n".join(menu_lines)
    print(menu_string)
    choice = input("Please choose an option: ")
    func = funcs.get(choice)
    if(func):
        func()
    else:
        print("Invalid option, please try again.")
        new_download()


def remove_show():
    os.system("cls")
    shows = search_show()
    if(len(shows) == 1):
        show = shows[0]
        print(f"Found show {show['showName']}")
        yn = input("Would you like to remove this show from the DB (y/n): ")
        if(yn == "y"):
            for season_oid in show["seasons"]:
                season = client.media.seasons.find_one({"_id": season_oid})
                if(season["fullSeasonDownload"]):
                    client.media.downloads.delete_one({"_id": season["fullSeasonDownload"]})
                # TODO: Implement non-full seasonal download removal
                client.media.seasons.delete_one({"_id": season_oid})
            client.media.shows.delete_one({"_id": show["_id"]})

    elif(len(shows > 1)):
        print(f"Found too many shows, please be more specific")
        for show in shows:
            print(show["showName"])
    


def main_menu():
    os.system("cls")
    choices = [
            "1) Add new show",
            "2) Add new download",
            "9) Remove Show"
        ]
    funcs = {
        "1": new_show,
        "2": new_download,
        "9": remove_show
        }
    choices_str = "\n".join(choices)
    main_menu_lines = [
                       "=== Splitt ===",
                       "Welcome to Splitt!",
                       "",
                       "=== Main Menu ===",
                       choices_str,
                       "==="*5]
    main_menu_str = "\n".join(main_menu_lines)
    print(main_menu_str)
    choice = input("Please choose an option: ")
    func = funcs.get(choice)
    if(func):
        func()
    else:
        print("Invalid option, please try again.")
        main_menu()


main_menu()
